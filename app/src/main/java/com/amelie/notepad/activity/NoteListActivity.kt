package com.amelie.notepad.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.amelie.notepad.R
import com.amelie.notepad.adapter.NoteAdapter
import com.amelie.notepad.model.Note
import com.amelie.notepad.utils.deleteNote
import com.amelie.notepad.utils.loadNotes
import com.amelie.notepad.utils.persistNote
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar

class NoteListActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var notes: MutableList<Note>
    lateinit var adapter: NoteAdapter
    lateinit var coordinator: CoordinatorLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_note_list)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        findViewById<FloatingActionButton>(R.id.create_note_fab).setOnClickListener(this)

        notes = loadNotes(this)

        adapter = NoteAdapter(notes, this)

        val recyclerView = findViewById<RecyclerView>(R.id.recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

        coordinator = findViewById(R.id.coordinator)
    }

    override fun onClick(view: View) {
        if(view.tag != null)
        {
            showNoteDetails(view.tag as Int)
        } else {
            when(view.id) {
                R.id.create_note_fab -> createNewnote()
            }
        }
    }

    fun showNoteDetails(noteIndex: Int) {
        val note = if(noteIndex < 0) Note() else notes[noteIndex]
        val intent = Intent(this, NoteDetailsActivity::class.java)
        intent.putExtra(NoteDetailsActivity.EXTRA_NOTE, note as Parcelable)
        intent.putExtra(NoteDetailsActivity.EXTRA_NOTE_INDEX, noteIndex)
        startActivityForResult(intent, NoteDetailsActivity.REQUEST_EDIT_NOTE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode != Activity.RESULT_OK || data == null){
            return
        }
        when (requestCode) {
            NoteDetailsActivity.REQUEST_EDIT_NOTE -> processNoteEditResult(data)
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    fun processNoteEditResult(data: Intent) {
        val noteIndex = data.getIntExtra(NoteDetailsActivity.EXTRA_NOTE_INDEX, -1)
        when (data.action) {
            NoteDetailsActivity.ACTION_SAVE_NOTE -> {
                val note = data.getParcelableExtra<Note>(NoteDetailsActivity.EXTRA_NOTE)
                saveNote(note, noteIndex)
            }
            NoteDetailsActivity.ACTION_DELETE_NOTE -> {
                deleteNote(noteIndex)
            }
        }
    }

    fun saveNote(note : Note, noteIndex: Int) {
        persistNote(this, note)
        if (noteIndex < 0) {
            notes.add(0, note)
        } else {
            notes[noteIndex] = note
        }
        adapter.notifyDataSetChanged()
    }

    fun deleteNote(noteIndex: Int) {
        if (noteIndex < 0) {
            return
        }
        val note = notes.removeAt(noteIndex)
        deleteNote(this, note)
        adapter.notifyDataSetChanged()
        Snackbar.make(coordinator, "Note deleted", Snackbar.LENGTH_SHORT).show()
    }

    fun createNewnote() {
        showNoteDetails(-1)
    }
}
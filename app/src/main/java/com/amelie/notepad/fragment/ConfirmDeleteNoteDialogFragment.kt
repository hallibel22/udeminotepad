package com.amelie.notepad.fragment

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.DialogFragment

class ConfirmDeleteNoteDialogFragment(val noteTitle: String = "") : DialogFragment() {

    interface ConfirmDeleteDialogListener {
        fun onDialogPositiveClick()
        fun onDialogNegativeClick()
    }
    var listener: ConfirmDeleteDialogListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val builder = AlertDialog.Builder(activity)
        builder.setMessage("Are you sure you want to delete the note titled $noteTitle ?")
            .setPositiveButton("Delete", DialogInterface.OnClickListener {dialog, id -> listener?.onDialogPositiveClick()})
            .setNegativeButton("Cancel", DialogInterface.OnClickListener {dialog, id -> listener?.onDialogNegativeClick()})

        return builder.create()
    }
}